## Jim Ntare | FullStack Software Engineer | www.ntare.xyz

Rwandan Software Engineer with a keen interest in Data Analytics and its power to transform and improve organizations. I have experience in technologies including ReactJs, NodeJs, D3js. Python programming libraries like Pandas and matplotlib. Outstanding oral and written communication skills with the ability to work well and excel under pressure. Exceptional leadership, organizational, interpersonal, analytical, and problem resolution skills. Ability to thrive in both independent and collaborative work environments.


## CONTACTS

* **Phone**: (250) 735-338 900
* **Email**: jim.ntare@gmail.com
* LinkedIn: @jimNtare
* Github: @Ntare22

## KEY SKILLS


### **Front End**:
* React JS, Redux
* Sass, TypeScript
* jQuery UI, JavaScript/ES6

### **Back End**:
* Express.js, Springboot
* NodeJS
### **Databases**:
* CRUD, MySQL
* MongoDB, ORD, ORM, PL/SQL
* POSTGRES, Sequelize

### **Testing**:
* TDD, Mocha, chai

### **SDLC**:
* Pivotal Tracker, Jira, Scrum
* UML, Slack, Git Version Control

## WORK HISTORY

### **Bank of Kigali, Software Engineer (Integrations)**, 09/2020 - Current | Kigali, Rwanda 
* Analysis, design and development of new features in collaboration with customers, business analysts and software partners
* Collaborating and contributing to projects across our wider Banking System
* Collaborating with our Architecture and Design Board
* Updating technical documentation using Swagger.js
* Integrating various external API's to the core banking system


### **Andela, Associate Software Engineer**, 11/2019 - 06/2020 | Kigali, Rwanda 
* Worked on various internal projects by the company including a travel management system for company employees
* Collaborating and contributing code to the backend using Node/express, Sequelize and other various dependencies
* Collaborating and contributing code to the frontend mainly using React/Redux.
* Worked on a few mockup designs for various pages mainly using Figma

### **SheCanCode Bootcamp, Technical Facilitator & Program Manager**, 08/2018 - 07/2019 | Kigali, Rwanda
* Designed and planned a curriculum for students admitted in the bootcamp.
* Prepared learning materials and organized learning activities for the students attending the bootcamp.
* Taught various technologies in Web development (Intro to Programming, ReactJs, NodeJs)


## SIDE PROJECTS


### **Rwanda Open Source, Member** | www.opensource.org.rw
* I work with the pioneer alpha team in the community to develop the official website for the Rwanda Open Source community. Mainly used Typescript throughout the
project, Node/Express forthe backend and React for Frontend

### **StockLoop, Developer**  
* Working with various colleagues to develop an inventory management system for various businesses that need an easier way of tracking inventory.

## EDUCATION

* Business Management. Southern New Hampshire University, 2016 - 2019. 
* Frontend Web Development Course with React. Scrimba. 

## ADDITIONAL INFORMATION

* Kinyarwanda: Native expertise
* English: Fluent expertise
* French: Third Language